package com.autenticacao;

import com.autenticacao.security.Usuario;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsuarioController {

    @GetMapping("/usuario")
    public Usuario obterUsuarioLogado(@AuthenticationPrincipal Usuario usuario){
        return usuario;
    }
}
